# docker-builds-redis-cluster

#### 介绍
docker搭建redis集群

#### 软件架构
docker-compose 搭建redis集群（三主三从）


#### 安装教程

1.  先参照`https://hub.docker.com/r/leaon/redis-cluster`网站中的步骤,我这边只是更换了redis.conf的配置.
2.  每个redis.conf配置文件下的宿主IP配置自己电脑的IP地址
3.  每个配置文件设置好端口后,在当前文件夹下打开命令创建输入`docker-compose up -d`;
4.  等各个redis节点构建好之后,复制`创建集群.txt`文件中的命令,将`192.168.0.155`,IP地址改为自己的IP地址。  
